<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit841c9b2cd2cf283083fba3aa0c78b8e6
{
    public static $prefixLengthsPsr4 = array (
        'D' => 
        array (
            'Dotenv\\' => 7,
        ),
        'A' => 
        array (
            'App\\' => 4,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'Dotenv\\' => 
        array (
            0 => __DIR__ . '/..' . '/vlucas/phpdotenv/src',
        ),
        'App\\' => 
        array (
            0 => __DIR__ . '/../..' . '/app',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit841c9b2cd2cf283083fba3aa0c78b8e6::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit841c9b2cd2cf283083fba3aa0c78b8e6::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}

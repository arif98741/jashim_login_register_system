<?php

require_once '../controllers/Users.php';
require_once '../helper/Session.php';


$user = new Users();
$data = $user->login();


$loginmsg = Session::get('loginmsg');
echo $loginmsg;

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Login</title>
    <link rel="stylesheet" href="../../../../public/bootstrap/css/bootstrap.min.css">
    <style type="text/css">
        body{ font: 14px sans-serif;}
        .card{
            width:340px;
            margin:50px auto;
        }
        .card form{
            margin-bottom:15px;
            background:#f7f7f7f;
            box-shadow: 0px 2px 2px rgba(0,0,0,0.4);
            padding:30px;
        }
        .card h2{
            margin:0 0 15px;
        }
    </style>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-6 mx-auto">
            <div class="card">
                <h2>Login</h2>
                <p>Please fill up with your credentials</p>
                
                <form action="login.php" method="POST">

                    <div class="form-group <?php echo (!empty($data['username_err'])) ? 'is-invalid' : ''; ?>">
                        <label>Username</label>
                        <input type="text" name="username" class="form-control">
                        <span class="invalid-feedback"><?php echo $data['username_err']; ?></span>
                    </div>
                    
                    <div class="form-group <?php echo (!empty($data['password_err'])) ? 'is-invalid' : ''; ?>">
                        <label>Password</label>
                        <input type="password" name="password" class="form-control">
                        <span class="invalid-feedback"><?php echo $data['password_err']; ?></span>
                    </div>
                    
                    <div class="row">
                       <div class="col">
                           <input type="submit" value="login" name="login" class="btn btn-success btn-block">
                       </div>
                       <div class="col">
                           <a href="register.php" class="btn btn-light btn-block">Don't have an account !! Register</a>
                       </div>
                    </div>

                    
                </form>
            </div>
        </div>
    </div>
</div>

</body>
</html>

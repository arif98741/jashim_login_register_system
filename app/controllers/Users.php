<?php


/**
 *  users Class
 *  Validate data for login and register system
 */


//  include '../inc/Database.php';
//
//
// class  Users
// {
// 	public $db;
//
// 	public function __construct(){
// 		$this->db = new Database();
// 	}
//
//
// 	public function register(){
// 		// Define variables and initialize with empty values
// 		$username = $password = $confirm_password = "";
// 		$username_err = $password_err = $confirm_password_err = "";
//
// 		// Processing form data when form is submitted
// 		if($_SERVER["REQUEST_METHOD"] == "POST"){
//
// 			// Validate username
// 			if(empty(trim($_POST["username"]))){
// 				$username_err = "Please enter a username.";
// 			} else{
// 				// Prepare a select statement
// 				$sql = "SELECT id FROM tbl_user WHERE username = :username";
//
// 				if($sth = $this->db->dbh->prepare($sql)){
// 					// Bind variables to the prepared statement as parameters
// 					$sth->bindParam(":username", $param_username, PDO::PARAM_STR);
//
// 					// Set parameters
// 					$param_username = trim($_POST["username"]);
//
// 					// Attempt to execute the prepared statement
// 					if($sth->execute()){
// 						if($sth->rowCount() == 1){
// 							$username_err = "This username is already taken.";
// 						} else{
// 							$username = trim($_POST["username"]);
// 						}
// 					} else{
// 						echo "Oops! Something went wrong. Please try again later.";
// 					}
// 				}
//
// 				// Close statement
// 				unset($sth);
// 			}
//
// 			// Validate password
// 			if(empty(trim($_POST["password"]))){
// 				$password_err = "Please enter a password.";
// 			} elseif(strlen(trim($_POST["password"])) < 3){
// 				$password_err = "Password must have atleast 3 characters.";
// 			} else{
// 				$password = trim($_POST["password"]);
// 			}
//
// 			// Validate confirm password
// 			if(empty(trim($_POST["confirm_password"]))){
// 				$confirm_password_err = "Please confirm password.";
// 			} else{
// 				$confirm_password = trim($_POST["confirm_password"]);
// 				if(empty($password_err) && ($password != $confirm_password)){
// 					$confirm_password_err = "Password did not match.";
// 				}
// 			}
//
// 			// Check input errors before inserting in database
// 			if(empty($username_err) && empty($password_err) && empty($confirm_password_err)){
//
// 				// Prepare an insert statement
// 				$sql = "INSERT INTO tbl_user (username, password) VALUES (:username, :password)";
//
// 				if($sth = $this->db->dbh->prepare($sql)){
// 					// Bind variables to the prepared statement as parameters
// 					$sth->bindParam(":username", $param_username, PDO::PARAM_STR);
// 					$sth->bindParam(":password", $param_password, PDO::PARAM_STR);
//
// 					// Set parameters
// 					$param_username = $username;
// 					$param_password = password_hash($password, PASSWORD_DEFAULT); // Creates a password hash
//
// 					// Attempt to execute the prepared statement
// 					if($sth->execute()){
// 						// Redirect to login page
// 						header("location: __DIR__.'/login.php'");
// 					} else{
// 						echo "Something went wrong. Please try again later.";
// 					}
// 				}
//
// 				// Close statement
// 				unset($sth);
// 			}
//
// 			// Close connection
// 			unset($pdo);
// 		}
// 	} // end of register method
//
// 	public function login() {
// 		$username = $password = "";
// 		$username_err = $password_err = "";
//
// 		// Process form data when form is submitted
// 		if($_SERVER['REQUEST_METHOD'] == "POST"){
// 			$_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
// 			// Check if username is empty
// 			if(empty(trim($_POST['username']))){
// 				$username_err = "Please enter username";
// 			} else{
// 				$username = trim($_POST['username']);
// 			}
//
// 			// Check if password is empty
// 			if(empty(trim($_POST['password']))){
// 				$password_err = "Please enter your password";
// 			} else {
// 				$password = trim($_POST['password']);
// 			}
//
// 			// Validate credentials
//
// 			if(empty($username_err) && empty($password_err)){
// 				// prepare a select statement
// 				$sql = "SELECT id, username, password FROM tbl_user WHERE username = :username";
// 				if($sth = $this->db->dbh->prepare($sql)){
// 					// bind value
// 					$sth->bindParam(":username", $param_username, PDO::PARAM_STR);
// 					// Set parameters
// 					$param_username = trim($_POST['username']);
//
// 					// Attempt to execute the prepared statement
// 					if($sth->execute()){
// 						// check if username exists , if yes then verify password
// 						if($sth->rowCount() == 1 ){
// 							if($row = $sth->fetch()){
// 								$id = $row['id'];
// 								$username = $row['username'];
// 								$hashed_password = $row['password'];
// 								if(password_verify($password, $hashed_password)){
// 									// password is correct , so start a new session
//
// 									Session::init();
//
// 									// Store data in session
// 									Session::set('login', true );
// 									Session::set('id', $id);
// 									Session::set('username', $username);
// 									Session::set('loginmsg', "<div class='alert alert-success'> Login successfull </div>");
//
// 									// Redirect user to welcome page
// 									header('location:../index.php');
// 								} else {
// 									$password_err = 'Password did not matched';
// 								}
// 							}
// 						}
// 					}
// 				} else {
// 					echo "Data did not matched";
// 				}
//
// 			} else {
// 				echo "Data not found";
// 			}
// } // end of form process
//
//
// 	} // End of login method
//
//
//
//
// }

<?php
/**
 * Base Controller
 * Loads models and views
 * Time: 6:29 PM
 */

namespace App\lib;


class Controller {

	public function model($model) {
		// require the model file
		require_once '../models/'. $model .' .php ';
		return new $model();
	}

	public function view($view , $data = []) {

		// require the views
		require_once '../views/'. $view. '.php';

		if (file_exists('../views/'.$view. '.php')){
			require_once '../views/'.$view. '.php';

		} else {
			die('view doesn\'t exists');
		}
	}
}